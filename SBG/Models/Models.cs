﻿namespace SBG.Models
{
    public class Models
    {
    }

    public class LogIn
    {
        public int id { get; set; }
        public string method { get; set; }

        public LogIn()
        {
            id = 1;
            method = "admin.login";
        }
    }

    public class JobCreate
    {
        public int id { get; set; }
        public string method { get; set; }
        public JobParams @params { get; set; }

        public JobCreate(DataModel model)
        {
            id = 2;
            method = "job.create";
            @params = new JobParams
            {
                customerName = "SBG",
                jobName = model.MembershipNumber,
                projectTemplateName = "SBG_Template",
                documentWorkflow = "SBGDocumentwfl",
                metadatas = new[]
                    {
                        new[] {"SBG", "Membership_Number", model.MembershipNumber},
                        new[] {"SBG", "Email_Address", model.Email},
                        new[]
                        {
                            "SBG", "Business_Address", model.BusinessName + ","
                                                       + model.BusinessLine1 + ","
                                                       + model.BusinessLine2 + ","
                                                       + model.BusinessCity + ","
                                                       + model.BusinessCounty + ","
                                                       + model.BusinessPostcode
                        },
                        new[] {"SBG", "Business_Email_Address", model.BusinessEmail},
                        new[] {"SBG", "Business_Telephone_Number", model.BusinessTelephone},
                        new[] {"SBG", "Website_Address", model.BusinessWebsiteAddress}
                    }
            };
        }
    }

    public class JobParams
    {
        public string customerName { get; set; }
        public string jobName { get; set; }
        public string projectTemplateName { get; set; }
        public string documentWorkflow { get; set; }
        public string[][] metadatas { get; set; }
    }

    public class DocumentCreate
    {
        public int id { get; set; }
        public string method { get; set; }
        public DocumentParams @params { get; set; }

        public DocumentCreate(string jobId, DataModel model)
        {
            id = 2;
            method = "document.create";
            @params = new DocumentParams
            {
                jobID = jobId,
                name = model.LogoName,
                documentWorkflow = "SBGDocumentwfl",
                URL = model.LogoUrl,
                moveFile = "false"
            };
        }
    }

    public class DocumentParams
    {
        public string jobID { get; set; }
        public string name { get; set; }
        public string documentWorkflow { get; set; }
        public string URL { get; set; }
        public string moveFile { get; set; }
    }

    public class LogOut
    {
        public int id { get; set; }
        public string method { get; set; }

        public LogOut()
        {
            id = 3;
            method = "admin.logout";
        }
    }

    public class JobResult
    {
        public int id { get; set; }
        public Result result { get; set; }
        public string method { get; set; }
    }

    public class Result
    {
        public string ID { get; set; }
        public string method { get; set; }
    }

    public class AssetUploadModel
    {
        public string MembershipNumber { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        public int DocumentQuantity { get; set; }
        public string BusinessName { get; set; }
        public string BusinessLine1 { get; set; }
        public string BusinessLine2 { get; set; }
        public string BusinessCity { get; set; }
        public string BusinessCounty { get; set; }
        public string BusinessPostcode { get; set; }
        public string BusinessTelephone { get; set; }
        public string BusinessEmail { get; set; }
        public string BusinessWebsiteAddress { get; set; }
        public string LogoName { get; set; }
        public string LogoUrl { get; set; }
        public int DeliveryPercentage { get; set; }
        public string SecondaryDeliveryName { get; set; }
        public string SecondaryDeliveryLine1 { get; set; }
        public string SecondaryDeliveryLine2 { get; set; }
        public string SecondaryDeliveryCity { get; set; }
        public string SecondaryDeliveryCounty { get; set; }
        public string SecondaryDeliveryPostcode { get; set; }
    }

    public class DataModel
    {
        public string MembershipNumber { get; set; }
        public string Email { get; set; }
        //public string Name { get; set; }
        public int DocumentQuantity { get; set; }
        public int BusinessDeliveryQuantity { get; set; }
        public int SecondaryDeliveryQuanity { get; set; }
        public string BusinessName { get; set; }
        public string BusinessLine1 { get; set; }
        public string BusinessLine2 { get; set; }
        public string BusinessCity { get; set; }
        public string BusinessCounty { get; set; }
        public string BusinessPostcode { get; set; }
        public string BusinessTelephone { get; set; }
        public string BusinessEmail { get; set; }
        public string BusinessWebsiteAddress { get; set; }
        public string LogoName { get; set; }
        public string LogoUrl { get; set; }
        //public int DeliveryPercentage { get; set; }
        public string SecondaryDeliveryName { get; set; }
        public string SecondaryDeliveryLine1 { get; set; }
        public string SecondaryDeliveryLine2 { get; set; }
        public string SecondaryDeliveryCity { get; set; }
        public string SecondaryDeliveryCounty { get; set; }
        public string SecondaryDeliveryPostcode { get; set; }

        public DataModel(AssetUploadModel model)
        {
            MembershipNumber = model.MembershipNumber;
            Email = model.Email;
            DocumentQuantity = model.DocumentQuantity;
            BusinessName = model.BusinessName;
            BusinessLine1 = model.BusinessLine1;
            BusinessLine2 = model.BusinessLine2;
            BusinessCity = model.BusinessCity;
            BusinessCounty = model.BusinessCounty;
            BusinessPostcode = model.BusinessPostcode;
            BusinessTelephone = model.BusinessTelephone;
            BusinessEmail = model.BusinessEmail;
            BusinessWebsiteAddress = model.BusinessWebsiteAddress;
            LogoName = model.LogoName;
            LogoUrl = model.LogoUrl;
            BusinessDeliveryQuantity = (model.DocumentQuantity / 100) * (100 - model.DeliveryPercentage);
            SecondaryDeliveryName = model.SecondaryDeliveryName;
            SecondaryDeliveryLine1 = model.SecondaryDeliveryLine1;
            SecondaryDeliveryLine2 = model.SecondaryDeliveryLine2;
            SecondaryDeliveryCity = model.SecondaryDeliveryCity;
            SecondaryDeliveryCounty = model.SecondaryDeliveryCounty;
            SecondaryDeliveryPostcode = model.SecondaryDeliveryPostcode;
            SecondaryDeliveryQuanity = (model.DocumentQuantity / 100) * model.DeliveryPercentage;
        }
    }
}