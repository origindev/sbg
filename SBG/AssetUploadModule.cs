﻿using System;
using System.IO;
using System.Web;
using Nancy;
using Nancy.ModelBinding;
using SBG.Models;

namespace SBG
{
    public class AssetUploadModule : NancyModule
    {
        public AssetUploadModule()
        {
            var baseFolder = HttpContext.Current.Server.MapPath("~/");
            var inputFile = string.Format("{0}input.csv", baseFolder);
            var outputFile = string.Format("{0}output.csv", baseFolder);

            Post["/post"] = _ =>
            {
                var model = this.Bind<AssetUploadModel>();
               
                var imageDetails = Factory.SaveFile(Request.Files, model.MembershipNumber, baseFolder);
                model.LogoName = imageDetails[0];
                model.LogoUrl = imageDetails[1];
                Factory.SendToEs(model);

                File.AppendAllText(outputFile,
                    string.Format(
                        "{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, {21}, {22}, {23}{24}",
                        model.MembershipNumber,
                        model.Email,
                        model.Name,
                        model.Telephone,
                        model.DocumentQuantity,
                        (model.DocumentQuantity / 100) * (100 - model.DeliveryPercentage),
                        (model.DocumentQuantity / 100) * model.DeliveryPercentage,
                        model.BusinessName,
                        model.BusinessLine1,
                        model.BusinessLine2,
                        model.BusinessCity,
                        model.BusinessCounty,
                        model.BusinessPostcode,
                        model.BusinessTelephone,
                        model.BusinessEmail,
                        model.BusinessWebsiteAddress,
                        model.LogoName,
                        model.LogoUrl,
                        model.SecondaryDeliveryName,
                        model.SecondaryDeliveryLine1,
                        model.SecondaryDeliveryLine2,
                        model.SecondaryDeliveryCity,
                        model.SecondaryDeliveryCounty,
                        model.SecondaryDeliveryPostcode,
                        Environment.NewLine));
                return View["thankyou.cshtml",model];
            };

            Get["/{membershipnumber}"] = parameters =>
            {             
                var model = Factory.BuildMember(parameters.membershipnumber, inputFile);
                return View["assetupload.cshtml",model];
            };
        }
    }
}