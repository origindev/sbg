﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using Nancy;
using Newtonsoft.Json;
using SBG.Models;


namespace SBG
{
    public class Factory
    {
        public static AssetUploadModel BuildMember(string membershipNumber, string filePath)
        {
            var model = new AssetUploadModel();

            var reader = new StreamReader(File.OpenRead(filePath));
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                if (line != null)
                {
                    var values = line.Split(',');
                    {
                        if (values[0] == membershipNumber)
                        {
                            model.MembershipNumber = values[0];
                            model.Email = values[1];
                            model.DocumentQuantity = Convert.ToInt32(values[2]);
                            return model;
                        }
                    }
                }
            }
            return model;
        }

        public static string[] SaveFile(IEnumerable<HttpFile> files, string fileName, string baseFolder)
        {
            var file = files.FirstOrDefault();
            var data = new string[2];
            if (file != null && file.Value.Length > 0)
            {
                var fullName = file.Name;
                var filetype = fullName.Substring(fullName.LastIndexOf('.'));
                data[0] = fileName + filetype;
                var siteUrl = ConfigurationManager.AppSettings["siteUrl"];
                data[1] = siteUrl + "/Content/images" + @"/" + fileName + filetype; ;
                var fullPath = baseFolder + "Content/images" + @"\" + fileName + filetype;
                using (var fileStream = File.Create(fullPath))
                {
                    file.Value.Seek(0, SeekOrigin.Begin);
                    file.Value.CopyTo(fileStream);
                }
                return data;
            }
            return data;
        }

        public static void SendToEs(AssetUploadModel model)
        {
            var dataModel = new DataModel(model);
            var login = new LogIn();
            var job = new JobCreate(dataModel);
            var logout = new LogOut();

            var loginJson = JsonConvert.SerializeObject(login);
            var jobJson = JsonConvert.SerializeObject(job);
            var logoutJson = JsonConvert.SerializeObject(logout);

            var jobCreateJson = "[" + loginJson + "," + jobJson + "," + logoutJson + "]";
            var jobCreateResult = HttpWebRequest(jobCreateJson, dataModel);

            
            var responseId = GetResponseIdFromJobResult(jobCreateResult);
            if (responseId == "") return;

            login = new LogIn();
            var document = new DocumentCreate(responseId, dataModel);
            logout = new LogOut();

            loginJson = JsonConvert.SerializeObject(login);
            var documentJson = JsonConvert.SerializeObject(document);
            logoutJson = JsonConvert.SerializeObject(logout);

            var createDocumentJson = "[" + loginJson + "," + documentJson + "," + logoutJson + "]";
            var documentCreateResult = HttpWebRequest(createDocumentJson, dataModel);
        }

        public static string GetResponseIdFromJobResult(string response)
        {
            var docCreationResultJson = JsonConvert.DeserializeObject(response);
            var docCreationResultJsonArray = ((IEnumerable)docCreationResultJson).Cast<object>().Select(x => x == null ? x : x.ToString()).ToArray();
            var resultArrayObject = docCreationResultJsonArray[1].ToString();
            var resultArray = JsonConvert.DeserializeObject<JobResult>(resultArrayObject);
            return resultArray.result == null ? "" : resultArray.result.ID;
        }

        public static string HttpWebRequest(string json, DataModel dataModel)
        {
            var baseFolder = HttpContext.Current.Server.MapPath("~/");
            var responseFile = string.Format("{0}ESresponses\\" + dataModel.MembershipNumber + "_" + DateTime.Now.ToString("-ddMMMyy-HHmmss") + ".csv", baseFolder);

            var esapiPath = ConfigurationManager.AppSettings["ESAPIPath"];
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(esapiPath);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.PreAuthenticate = true;
            httpWebRequest.Credentials = new NetworkCredential("admin", "admin");

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            string jsonResult;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                jsonResult = result;
                File.AppendAllText(responseFile, result);
            }
            return jsonResult;
        }      
    }
}